#Maintainer: GINU MATHEW
###apache
Ansible role which helps to install and configure Apache web server.

The configuraton of the role is done in such way that it should not be necessary to change the role for any kind of configuration. All can be done either by changing role parameters or by declaring completely new configuration as a variable. That makes this role absolutely universal. See the examples below for more details.